#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import ConfigParser
from hermes_python.hermes import Hermes
from hermes_python.ontology import *
import io

CONFIGURATION_ENCODING_FORMAT = "utf-8"
CONFIG_INI = "config.ini"

class SnipsConfigParser(ConfigParser.SafeConfigParser):
    def to_dict(self):
        return {section : {option_name : option for option_name, option in self.items(section)} for section in self.sections()}


def read_configuration_file(configuration_file):
    try:
        with io.open(configuration_file, encoding=CONFIGURATION_ENCODING_FORMAT) as f:
            conf_parser = SnipsConfigParser()
            conf_parser.readfp(f)
            return conf_parser.to_dict()
    except (IOError, ConfigParser.Error) as e:
        return dict()

def subscribe_intent_callback(hermes, intentMessage):
    conf = read_configuration_file(CONFIG_INI)
    action_wrapper(hermes, intentMessage, conf)


def action_wrapper(hermes, intentMessage, conf):
    from datetime import datetime, date
    
    ereignis = str(intentMessage.slots.Ereignis.first().value).lower()
    
    if ereignis in [x.lower() for x in conf['secret']]:
      t = date.today()
      event_date = conf['secret'][ereignis]
      event_date_full = datetime.strptime(event_date,"%d.%m.").replace(year=t.year)
      d = (event_date_full-datetime(t.year,t.month,t.day)).days
      if ( d < 0 ):
        event_date_full = event_date_full.replace(year=t.year+1)
        d = (event_date_full-datetime(t.year,t.month,t.day)).days
      
      if ( d == 0 ):
        date_="heute"
      elif ( d == 1 ):
        date_="morgen"
      elif ( d == -1 ):
        date_="gestern"
      elif ( d == 2 ):
        date_="übermorgen"
      elif ( d == -2 ):
        date_="vorgestern"
      elif ( d < 0):
        date_="vor {} Tagen".format(-d)
      elif (d > 0):
        date_="in {} Tagen".format(d)
      else:
        date_="mir nicht klar, wann das sein soll"
      result_sentence = "{} ist der {}. Das ist {}".format(ereignis,event_date,date_)

    else:
      date_ = "ist mir nicht bekannt"
      result_sentence = "{} {}".format(ereignis,date_)
    
    current_session_id = intentMessage.session_id
    hermes.publish_end_session(current_session_id, result_sentence)
    


if __name__ == "__main__":
    with Hermes("localhost:1883") as h:
        h.subscribe_intent("daLiFe:Ereignis", subscribe_intent_callback) \
         .start()
